const menuBTN = document.querySelector(".header__btn");
const munuList = document.querySelector(".header__menu");
const imgTick = document.querySelector(".tick");
const imgCross = document.querySelector(".cross");

menuBTN.addEventListener("click", function() {
    munuList.classList.toggle("menu--active");
    if (munuList.classList.contains("menu--active")) {
        imgTick.classList.add("hidden");
        imgCross.classList.remove("hidden");
    } else {
        imgCross.classList.add("hidden");
        imgTick.classList.remove("hidden");
    }
});